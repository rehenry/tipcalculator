# Pre-work - Tip Calculator 

Tip Calculator is a tip calculator application for iOS.

Submitted by: Rebecca Henry

Time spent: 8 hours spent in total

## User Stories

The following **required** functionality is complete:
* [x] User can enter a bill amount, choose a tip percentage, and see the tip and total values.

The following **optional** features are implemented:
* [x] Settings page to change the default tip percentage.
* [x] UI animations
* [ ] Remembering the bill amount across app restarts (if <10mins)
* [ ] Using locale-specific currency and currency thousands separators.
* [x] Making sure the keyboard is always visible and the bill amount is always the first responder. This way the user doesn't have to tap anywhere to use this app. Just launch the app and start typing.

The following **additional** features are implemented:

- [x] App is able to split the bill total among up to 4 parties.

## Video Walkthrough 

Here's a walkthrough of implemented user stories:

<img src='http://imgur.com/vKrJug8.gif' title='Video Walkthrough' width='' alt='Video Walkthrough' />

GIF created with [LiceCap](http://www.cockos.com/licecap/).

## Notes

Building the initial app was pretty simple but integrating the optionals provided a challenge because I'm not experienced in programming or Swift. I asked for help and played around with things in my app to figure some of the optionals out. I had difficulty merging my updated files to my original repo so this one is for the updated project. The original repo is at https://github.com/rehenry/Tip-Calculator.

## License

    Copyright [2015] [Rebecca Henry]

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
