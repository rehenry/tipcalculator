//
//  SettingsViewController.swift
//  Tip Calculator 2
//
//  Created by Rebecca Henry on 12/31/15.
//  Copyright © 2015 Rebecca Henry. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {


    @IBOutlet weak var tipDefault: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Do any additional setup after loading the view.
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        print("view will appear")
        // This is a good place to retrieve the default tip percentage from NSUserDefaults
        // and use it to update the tip amount
        
        let defaults = NSUserDefaults.standardUserDefaults()
        
        let defaultTipIndex = defaults.integerForKey("default_tip_index")
        
        tipDefault.selectedSegmentIndex = defaultTipIndex
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        print("view did appear")
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        print("view will disappear")
        
        let defaults = NSUserDefaults.standardUserDefaults()
        
        let defaultTipIndex = tipDefault.selectedSegmentIndex
        
        defaults.setInteger(defaultTipIndex, forKey: "default_tip_index")
        
        defaults.synchronize()
        
        print("selected tip at index \(tipDefault.selectedSegmentIndex)")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */


